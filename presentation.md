---
title: Gherkin & BDD
author: multi.coop
date: 8 février 2024
title-slide-attributes:
    data-background-image: "static/logo.svg"
    data-background-size: "auto 10%"
    data-background-position: "95% 95%"
---

# Contexte : les spec’

Démarrer un produit et définir ses fonctionnalités n'est pas nécessairement évident.
Surtout si on démarre d'une page blanche.

## Plusieurs casquettes intéressées

1. La casquette **produit** 👩‍🏫
2. La casquette **qa** 🕵
3. La casquette **dev** 👩🏾‍💻

Chacun a ses attentes.

## Certaines informations sont cruciales

### Exemple de spec trop implicite (🚨 caricature 🚨) :

> Je veux pouvoir récupérer la liste de mes commandes

- C'est sans doute évident mais … il faut avoir un compte et être connecté
- Idem … il faut cliquer sur un bouton dédié sur son tableau de bord

# Quelques solutions

## Se réunir

On réunit un maximum de casquettes (et pas forcément un maximum de monde) pour déterminer des cas utilisateurs.
Une vision concrète des fonctionnalités est partagée à l'oral, puis à l'écrit…

Certains appellent cela le "three amigos" 🍕

Réunir plusieurs métiers c'est bien, avoir un format pour écrire c'est aller plus loin.
Une démarche appelée "Example mappings" suggère un échange pratique et concret.

## Example mappings

::: columns

::: column
Ce format utilise 4 couleurs :

1. *Jaune* la **story** décrit la fonctionnalité dans son ensemble
2. *Bleue* les **critères d’acceptances**
3. *Verte* les **exemples** / scénarios
4. *Rouge* les questions **sans réponses**
:::

::: column
![](images/map-of-stories.png){width=600px}
:::

:::
Un bon dégrossissement est fait avec ça 🧹

## Un format de scénario 👉🏿 Gherkin

::: columns
::: column
🇬🇧

1. **Given** I have need
2. **When** I use the product
3. **Then** my need is satisfied
:::


::: column
🇫🇷

1. **Étant donné** que j'ai un besoin
2. **Lorsque** j’utilise le produit
3. **Alors** mon besoin est comblé
:::
:::

On va décrire non pas la fonctionnalité mais un cas d'usage

## En détails

### Given / Étant donnée

Le **contexte** : connexion, précédente navigation, tout pré-requis indispensable pour utiliser la fonctionnalité.

### When / Lorsque

L’**action utilisateur** pour déclencher la fonctionnalité.

### Then / Alors

Le **service rendue** par la fonctionnalité, le résultat.

## Aller plus loin avec la syntaxe

- `And, But, * (liste puce)` : Pour rester toujours naturel dans la rédaction
- `Background` : Contexte commun aux scénarios (ex: compte connecté…)
- `Outline + Examples` : Pour permettre de jouer le même test avec plusieurs jeux de données, on fournit un tableau, exemple :
```
Scenario Outline: eating
  Given there are <start> cucumbers
  When I eat <eat> cucumbers
  Then I should have <left> cucumbers

  Examples:
    | start | eat | left |
    |    12 |   5 |    7 |
    |    20 |   5 |   15 |
```

## Avec un framework de test 🫱🏼‍🫲🏿

![](https://cdn.freebiesupply.com/logos/large/2x/cucumber-logo-png-transparent.png){width=20} **Cucumber** (python, java, javascript, ruby) ou ![](https://behat.org/en/latest/_static/images/b@2x.png){width=20} **Behat** (php)
vont faire le pont entre nos phrases / actions et du code via des *définitions*.


## Extrait

Notre **étape** de scénario
```feature
Given I am on the multi website
```

Notre **définition** de scénario
```javascript
Given('I am on the multi website', async function () {
    await driver.get('https://www.multi.coop')

    await driver.wait(
        until.elementLocated(
            By.id('multi-site-app-top')
        ),
        10000
    )
})
```

## Démarche BDD

Et ce système nous permet d'adopter une démarche de *Behaviour Driven Development*.
C'est à dire un développement qui sera guidé par des tests fonctionnels* :

1. Nous écrivons d'abords ces scénarios qui sont des specs, tests (et documentations).
    Feu rouge ! Les tests échouent 🟥 car la fonctionnalité n'est pas encore développée.
2. On développe, les feux passent au 🟩 = la fonctionnalité est en bonne voie 🎊

# Demo cucumber/js

![Ça risque de péter](images/demo-time.jpg)

# Conclusion

## Mon opinion

::: columns

::: column
### Pour 🥰
- Réunion pour définir les specs
- Langage vraiment naturel
- Possible en français et autres
- Très adaptés test *e2e*
- C'est amusant (surtout quand c'est fini)
:::

::: column
### Contre 🤬
- Tests *e2e* très fragiles
(beaucoup de vérifications côté navigateur)
- Test *e2e* longs à s'exécuter
- Pas très adaptés aux tests d'intégration (comparaison de hash…)
:::

:::

# Merci
